// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "TestSDKGitlab",
    platforms: [
        .iOS("13.0")
    ],
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "TestSDKGitlab",
            targets: ["TestSDKGitlab"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .binaryTarget(
            name: "TestSDKGitlab",
            url: "https://gitlab.com/api/v4/projects/26427609/packages/generic/SDK/0.0.1/TestSDKGitlab.xcframework.zip",
            checksum: "ba4e623f3f01e7c442294ab13cdd49fd0fae3e94b4185fdaafc7bdd384db4c0c")
    ]
)
